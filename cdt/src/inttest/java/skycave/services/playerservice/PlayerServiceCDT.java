package skycave.services.playerservice;

import com.google.gson.Gson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlayerServiceCDT {

    public class PlayerRecord {
        private final String id;
        private final String name;
        private final String group;
        private final String region;
        private final String accessToken;
        private final String position;

        public PlayerRecord(String id, String name, String group, String region, String position, String accessToken) {
            this.id = id;
            this.name = name;
            this.group = group;
            this.region = region;
            this.position = position;
            this.accessToken = accessToken;
        }

        public String getId() { return id; }

        public String getName() { return name; }

        public String getGroup() { return group; }

        public String getRegion() { return region; }

        public String getAccessToken() { return accessToken; }

        public String getPosition() { return position; }
    }

    public class RequestError {
        private String errorMessage;

        public RequestError(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage() { return errorMessage; }
    }

    private interface StatusCode {
        int OK = 200;
        int Created = 201;
        int NotFound = 404;
        int BadRequest = 400;
    }

    private final static String MimeTypeJson = "application/json";

    private static GenericContainer playerServiceContainer;
    private static String serverRootUrl;

    @Before
    public void setup()
    {
        int port = 5000;
        playerServiceContainer = new GenericContainer("annxii/skycave-playerservice:latest")
                .withExposedPorts(port);

        playerServiceContainer.start();

        String address = playerServiceContainer.getContainerIpAddress();
        Integer exposedPort = playerServiceContainer.getMappedPort(port);
        serverRootUrl = "http://" + address + ":" + exposedPort + "/skycave/v1/";
    }

    @After
    public void cleanup() {
        playerServiceContainer.stop();
    }

    private String createPlayerStateUri(String id) {
        return serverRootUrl + "players/" + id;
    }

    private String createPlayersAtUri(String position) {
        return serverRootUrl + "playersat/" + position;
    }

    private <T> T deserialize(HttpResponse<String> reply, Class<T> classOfT) {
        return new Gson().fromJson(reply.getBody(), classOfT);
    }

    @Test
    public void givenPlayerDoesNotExist_whenSavingPlayerState_thenReturnNewStateCreated() throws UnirestException {
        // arrange
        String id = "966b2070-b5e1-4583-88d6-ad6bd5345c4f";
        PlayerRecord player = new PlayerRecord(
                id,
                "Torsten",
                "echo",
                "AARHUS",
                "(7,9,13)",
                "906dd268-484a-40e0-b45b-b965e4ed33e9"
        );

        // act
        HttpResponse<String> reply = Unirest
                .post(createPlayerStateUri(id))
                .contentType(MimeTypeJson)
                .body(player)
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.Created));

        PlayerRecord fromService = deserialize(reply, PlayerRecord.class);
        assertThat(player.getId(), is(fromService.getId()));
        assertThat(player.getName(), is(fromService.getName()));
        assertThat(player.getGroup(), is(fromService.getGroup()));
        assertThat(player.getRegion(), is(fromService.getRegion()));
        assertThat(player.getPosition(), is(fromService.getPosition()));
        assertThat(player.getAccessToken(), is(fromService.getAccessToken()));
    }

    @Test
    public void givenPlayerExists_whenRequestingPlayerState_thenReturnStateAndOk() throws UnirestException {
        // arrange
        String id = "2a69499d-b22f-40d2-b8c4-c92ae8687cac";
        PlayerRecord player = new PlayerRecord(
                id,
                "Torsten",
                "echo",
                "AARHUS",
                "(7,9,13)",
                "bc32eb6f-78a1-49c3-b7e6-b16bcb0ba5a0"
        );

        // ensuring player state exists...
        HttpResponse<String> reply = Unirest
                .post(createPlayerStateUri(id))
                .contentType(MimeTypeJson)
                .body(player)
                .accept(MimeTypeJson)
                .asString();

        // act
        reply = Unirest
                .get(createPlayerStateUri(id))
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.OK));

        PlayerRecord fromService = deserialize(reply, PlayerRecord.class);
        assertThat(player.getId(), is(fromService.getId()));
        assertThat(player.getName(), is(fromService.getName()));
        assertThat(player.getGroup(), is(fromService.getGroup()));
        assertThat(player.getRegion(), is(fromService.getRegion()));
        assertThat(player.getPosition(), is(fromService.getPosition()));
        assertThat(player.getAccessToken(), is(fromService.getAccessToken()));
    }

    @Test
    public void whenRequestingUnknownPlayer_thenReturnNotFound() throws UnirestException {
        // arrange

        // act
        HttpResponse<String> reply = Unirest
                .get(createPlayerStateUri("dummy"))
                .accept(MimeTypeJson)
                .asString();;

        // assert
        assertThat(reply.getStatus(), is(StatusCode.NotFound));

        RequestError result = deserialize(reply, RequestError.class);
        assertThat(result.getErrorMessage(), notNullValue());
    }

    @Test
    public void givenInvalidPosition_thenReturnBadRequest() throws UnirestException {
        // arrange
        String id = "8ca45dcc-6c86-4d6e-9f93-d9fd62df4331";
        String position = "(a,b,c)";
        PlayerRecord player = new PlayerRecord(
                id,
                "Torsten",
                "echo",
                "AARHUS",
                position,
                "7d882dcd-f88c-4346-8d22-bc9b386d6777"
        );

        // act
        HttpResponse<String> reply = Unirest
                .post(createPlayerStateUri(id))
                .contentType(MimeTypeJson)
                .body(player)
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.BadRequest));

        RequestError result = deserialize(reply, RequestError.class);
        assertThat(result.getErrorMessage(), notNullValue());
    }

    @Test
    public void givenMissingId_thenReturnBadRequest() throws UnirestException {
        // arrange
        String id = "46f9f99f-c24a-4928-baa1-0c1bb001fb20";
        PlayerRecord player = new PlayerRecord(
                null,
                "Torsten",
                "echo",
                "AARHUS",
                "(1,2,3)",
                "6e20b6e4-4c07-47ae-8a27-84b54311a79f"
        );

        // act
        HttpResponse<String> reply = Unirest
                .post(createPlayerStateUri(id))
                .contentType(MimeTypeJson)
                .body(player)
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.BadRequest));

        RequestError result = deserialize(reply, RequestError.class);
        assertThat(result.getErrorMessage(), notNullValue());
    }

    @Test
    public void givenMissingName_thenReturnBadRequest() throws UnirestException {
        // arrange
        String id = "2c3be050-c244-436e-8c7c-f0cfc4d98d71";
        PlayerRecord player = new PlayerRecord(
                id,
                null,
                "echo",
                "AARHUS",
                "(1,2,3)",
                "a242b2c7-7152-4739-bc87-184d9eb77d98"
        );

        // act
        HttpResponse<String> reply = Unirest
                .post(createPlayerStateUri(id))
                .contentType(MimeTypeJson)
                .body(player)
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.BadRequest));

        RequestError result = deserialize(reply, RequestError.class);
        assertThat(result.getErrorMessage(), notNullValue());
    }

    @Test
    public void givenPlayersAtPosition_thenReturnPlayersAndOk() {
        // arrange
        String position = "(7,9,13)";
        PlayerRecord player1 = new PlayerRecord(
                "2c3be050-c244-436e-8c7c-f0cfc4d98d71",
                "Torsten",
                "echo",
                "AARHUS",
                position,
                "a242b2c7-7152-4739-bc87-184d9eb77d98"
        );

        PlayerRecord player2 = new PlayerRecord(
                "0277f3d4-7073-4880-a3b3-055d61b95977",
                "Henrik",
                "",
                "AARHUS",
                position,
                "a571dd58-1db4-4e4a-86e8-b3a688786b52"
        );

        // seeding players
        Unirest
                .post(createPlayerStateUri(player1.id))
                .contentType(MimeTypeJson)
                .body(player1)
                .accept(MimeTypeJson)
                .asString();

        Unirest
                .post(createPlayerStateUri(player2.id))
                .contentType(MimeTypeJson)
                .body(player2)
                .accept(MimeTypeJson)
                .asString();

        // act
        HttpResponse<String> reply = Unirest
                .get(createPlayersAtUri(position))
                .accept(MimeTypeJson)
                .asString();

        // assert
        assertThat(reply.getStatus(), is(StatusCode.OK));

        PlayerRecord[] playersAt = deserialize(reply, PlayerRecord[].class);
        assertThat(playersAt.length, is(2));

        Optional<PlayerRecord> player1FromService = Arrays.stream(playersAt).filter(x -> x.id.equals(player1.getId())).findFirst();
        assertThat(player1FromService.isPresent(), is(true));

        Optional<PlayerRecord> player2FromService = Arrays.stream(playersAt).filter(x -> x.id.equals(player2.getId())).findFirst();
        assertThat(player2FromService.isPresent(), is(true));
    }
}
