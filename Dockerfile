#Compile
FROM henrikbaerbak/jdk11-gradle68 AS build

WORKDIR /root

COPY ["/service/build.gradle", "/gradle.properties", "./"]
COPY ["/service/src", "./src"]

RUN ["gradle", "fatJar"]

FROM openjdk:11.0.12-jre-slim AS runtime-setup

RUN apt-get -qq update && \
    apt-get -qq install httpie && \
    rm -rf /var/cache/apt/*

COPY --from=build /root/build/libs/PlayerService.jar /root

# Runtime
FROM runtime-setup AS execution

LABEL maintainer="Team Echo"
EXPOSE 5000/tcp

WORKDIR /root

CMD ["java", "-jar", "PlayerService.jar"]