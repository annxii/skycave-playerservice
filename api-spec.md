# PlayerService API Specification

*Group: Echo*

- All payloads, both requests and responses, are JSON formats (*application/json*)
- Incorrectly formatted JSON requests will result in a *400 Bad Request*
- *Positions* are represented as coordinates in the string format "(x,y,z)"
  - **ex: "(1,-6,10)"**

## Source Code
Source code for *PlayerService* and associated consumer-driven tests are located at *BitBucket*

```
https://bitbucket.org/annxii/skycave-playerservice
```

Consumer-driven tests:
```
/cdt/src/inttest/java/skycave/services/playerservice/PlayerServiceCDT.java
```

## Docker image
A docker image with a *PlayerService* is made available at Docker Hub

The PlayerService runs on port *5000* with an *in-memory* storage.

**Pull:**
```
docker pull annxii/skycave-playerservice:latest
```
**Usage:**
```
docker run -d -p 5000:5000 --name skycave-playerservice annxii/skycave-playerservice
```

---
## Get player by ID
*Request the state of a player*

GET /skycave/v1/players/:{id}

### Responses:
> Status: 200 OK
> 
>```json
>{
>    "id": "376309f1-e3a5-48d5-bc64-f29a27b9e1da",
>    "name": "Torsten",
>    "group": "echo",
>    "region": "AARHUS",
>    "accessToken": "2f6290eb-39cf-4a31-b310-040fe7ab6604",
>    "position": "(1,2,3)"
>}
>```

> Status: 404 Not Found
> ```json
> {
>     "errorMessage": "<error description>"
> }
> ```

---
## Update player
*Create or update the state of a player*

POST /skycave/v1/players/:{id}

```json
{
    "id": "376309f1-e3a5-48d5-bc64-f29a27b9e1da",
    "name": "Torsten",
    "group": "echo",
    "region": "AARHUS",
    "accessToken": "2f6290eb-39cf-4a31-b310-040fe7ab6604",
    "position": "(1,2,3)"
}
```

### Responses:
> Status: 200 OK
> 
> ```json
> {
>   "id": "376309f1-e3a5-48d5-bc64-f29a27b9e1da",
>   "name": "Torsten",
>   "group": "echo",
>   "region": "AARHUS",
>   "accessToken": "2f6290eb-39cf-4a31-b310-040fe7ab6604",
>   "position": "(1,2,3)"
> }
> ```

> Status: 201 Created
>
> ```json
> {
>   "id": "376309f1-e3a5-48d5-bc64-f29a27b9e1da",
>   "name": "Torsten",
>   "group": "echo",
>   "region": "AARHUS",
>   "accessToken": "2f6290eb-39cf-4a31-b310-040fe7ab6604",
>   "position": "(1,2,3)"
> }
> ```

> Status: 400 Bad Request
> ```json
> {
>     "errorMessage": "<error description>"
> }
> ```

### Comments
Required fields: *id*, *name*, *position*

---
## Get player at position
*Request all players at a given position*

GET /skycave/v1/playersat/:{position}

### Responses:
> Status: 200 OK
> ```json
> [
>     {
>         "id": "376309f1-e3a5-48d5-bc64-f29a27b9e1da",
>         "name": "Torsten",
>         "group": "echo",
>         "region": "AARHUS",
>         "accessToken": "2f6290eb-39cf-4a31-b310-040fe7ab6604",
>         "position": "(1,2,3)"
>     },
>     {
>         "id": "42f653f9-c082-428c-a27f-b3951b5498a0",
>         "name": "Henrik",
>         "group": "<none>",
>         "region": "AARHUS",
>         "accessToken": "adedfd20-2edc-4e09-89a5-1ede73f47fcd",
>         "position": "(1,2,3)"
>     },
>     ...
> ]
> ```

> Status: 400 Bad Request
> ```json
> {
>     "errorMessage": "<error description>"
> }
> ```