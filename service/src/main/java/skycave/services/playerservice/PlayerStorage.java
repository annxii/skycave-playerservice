package skycave.services.playerservice;

import java.util.List;

public interface PlayerStorage {
    PlayerRecord getPlayerByID(String id) throws ValidationException;
    List<PlayerRecord> getPlayersAtPosition(String position) throws ValidationException;
    int updatePlayer(PlayerRecord player) throws ValidationException;
}
