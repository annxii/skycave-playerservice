package skycave.services.playerservice;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
