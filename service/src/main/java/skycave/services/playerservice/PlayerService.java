package skycave.services.playerservice;

import java.util.List;

public interface PlayerService {
    PlayerRecord getPlayerByID(String id) throws Exception;
    List<PlayerRecord> getPlayersAtPosition(String position) throws Exception;
    SavePlayerResponse savePlayer(PlayerRecord player) throws Exception;
}
