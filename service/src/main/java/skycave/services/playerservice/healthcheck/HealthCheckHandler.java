package skycave.services.playerservice.healthcheck;

import spark.Request;
import spark.Response;

public interface HealthCheckHandler {
    void start(String serviceName);
    Object getCurrentHealth(Request request, Response response) throws Exception;
}
