package skycave.services.playerservice.healthcheck;

import skycave.services.playerservice.doubles.DelayService;
import spark.Request;
import spark.Response;

import javax.servlet.http.HttpServletResponse;
import java.time.*;

public class DefaultHealthCheckHandler implements HealthCheckHandler {
    private final DelayService delayService;

    private class HealthCheckResponse {
        public final String serviceName;
        public final long uptime;

        public HealthCheckResponse(String serviceName, long uptime) {
            this.serviceName = serviceName;
            this.uptime = uptime;
        }
    }


    private String serviceName = "";
    private LocalDateTime startTime = LocalDateTime.MIN;

    public DefaultHealthCheckHandler(DelayService delayService) {
        this.delayService = delayService;
    }

    @Override
    public void start(String serviceName) {
        this.serviceName = serviceName;
        startTime = LocalDateTime.now();
    }

    @Override
    public Object getCurrentHealth(Request request, Response response) throws Exception {
        delayService.delay();
        response.status(HttpServletResponse.SC_OK);
        return createResponse();
    }

    private  HealthCheckResponse createResponse() {
        LocalDateTime now = LocalDateTime.now();

        // calc uptime
        Duration uptime = Duration.between(startTime, now);

        return new HealthCheckResponse(serviceName, uptime.toSeconds());
    }
}
