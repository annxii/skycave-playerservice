package skycave.services.playerservice;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.exceptions.JedisException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

public class RedisPlayerStorage implements PlayerStorage {

    private class RedisPlayerStorageWrapper implements PlayerStorage {
        private final Jedis jedis;

        public RedisPlayerStorageWrapper(Jedis jedis) {
            this.jedis = jedis;
        }

        @Override
        public PlayerRecord getPlayerByID(String id) {
            String playerKey = getPlayerKey(id);
            String data = jedis.get(playerKey);
            if (data == null) {
                return null;
            }

            return gson.fromJson(data, PlayerRecord.class);
        }

        @Override
        public List<PlayerRecord> getPlayersAtPosition(String position) {
            String playersInRoomKey = getPlayersInRoomKey(position);
            Set<String> playerIds = jedis.smembers(playersInRoomKey);

            if (playerIds == null || playerIds.size() == 0) {
                return emptyList();
            }

            // map to individual keys
            String[] playerKeys = playerIds
                    .stream()
                    .map(x -> getPlayerKey(x))
                    .toArray(String[]::new);

            // get and filter
            return jedis.mget(playerKeys)
                    .stream()
                    .map(x -> gson.fromJson(x, PlayerRecord.class))
                    .collect(Collectors.toList());
        }

        public int updatePlayer(PlayerRecord player) {
            String playerId = player.getId();
            String playerKey = getPlayerKey(playerId);

            // capture state before update
            PlayerRecord existingRecord = getPlayerByID(playerId);

            Transaction transaction = jedis.multi();
            try {
                // register is this is an update or a new creation
                transaction.exists(playerKey);

                // update the player
                String json = gson.toJson(player);
                transaction.set(playerKey, json);

                // update player position
                String newPlayersInRoomKey = getPlayersInRoomKey(player.getPosition());
                transaction.sadd(newPlayersInRoomKey, playerId);

                // update player position index
                if (existingRecord != null) {
                    String previousPlayersInRoomKey = getPlayersInRoomKey(existingRecord.getPosition());
                    // remove the player from previous room index if the player did in fact move
                    if (!newPlayersInRoomKey.equals(previousPlayersInRoomKey)) {
                        transaction.srem(previousPlayersInRoomKey, playerId);
                    }
                }

                List<Object> result = transaction.exec();
                if ((Boolean)result.get(0)) {
                    return HttpServletResponse.SC_OK;
                }
                else {
                    return HttpServletResponse.SC_CREATED;
                }
            }
            catch (Exception ex) {
                transaction.discard();
                throw ex;
            }
        }

        private String getPlayersInRoomKey(String positionString) {
            return "playersInRoom-" + positionString;
        }

        private String getPlayerKey(String playerId) {
            return "player-" + playerId;
        }

    }

    private static final Gson gson = new Gson();

    private final JedisPool pool;

    public RedisPlayerStorage(String host, int port) {
        this.pool = new JedisPool(host, port);
    }

    @Override
    public PlayerRecord getPlayerByID(String id) throws ValidationException {
        return callRedis(x -> x.getPlayerByID(id));
    }

    @Override
    public List<PlayerRecord> getPlayersAtPosition(String position) throws ValidationException {
        return callRedis(x -> x.getPlayersAtPosition(position));
    }

    @Override
    public int updatePlayer(PlayerRecord player) throws ValidationException {
        return callRedis(x -> x.updatePlayer(player));
    }

    private <T> T callRedis(Func<RedisPlayerStorageWrapper, T> executable) throws ValidationException {
        Jedis jedis = pool.getResource();
        try {
            RedisPlayerStorageWrapper wrapper = new RedisPlayerStorageWrapper(jedis);
            return executable.apply(wrapper);
        }
        catch (JedisException ex) {
            if(jedis != null) {
                pool.returnBrokenResource(jedis);
                jedis = null;
            }
            throw ex;
        }
        finally {
            if (jedis != null) {
                pool.returnResource(jedis);
            }
        }
    }

    @FunctionalInterface
    private interface Func<T, R> {
        R apply(T t) throws ValidationException;
    }

}
