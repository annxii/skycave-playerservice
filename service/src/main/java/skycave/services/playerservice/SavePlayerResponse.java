package skycave.services.playerservice;

public class SavePlayerResponse {
    private final int statusCode;
    private final PlayerRecord player;

    public SavePlayerResponse(int statusCode, PlayerRecord player) {
        this.statusCode = statusCode;

        this.player = player;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public PlayerRecord getPlayer() {
        return player;
    }
}
