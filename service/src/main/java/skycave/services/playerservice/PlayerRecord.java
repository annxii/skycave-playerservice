package skycave.services.playerservice;

public class PlayerRecord {
    private final String id;
    private final String name;
    private final String group;
    private final String region;
    private final String accessToken;
    private final String position;

    public PlayerRecord(String id, String name, String group, String region, String position, String accessToken) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.region = region;
        this.position = position;
        this.accessToken = accessToken;
    }

    public String getId() { return id; }

    public String getName() { return name; }

    public String getGroup() { return group; }

    public String getRegion() { return region; }

    public String getAccessToken() { return accessToken; }

    public String getPosition() { return position; }
}
