package skycave.services.playerservice.main;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skycave.services.playerservice.DefaultPlayerService;
import skycave.services.playerservice.PlayerService;
import skycave.services.playerservice.PlayerStorage;
import skycave.services.playerservice.RedisPlayerStorage;
import skycave.services.playerservice.doubles.DefaultDelayService;
import skycave.services.playerservice.doubles.DelayService;
import skycave.services.playerservice.doubles.FakePlayerStorage;
import skycave.services.playerservice.doubles.SaboteurPlayerService;
import skycave.services.playerservice.healthcheck.DefaultHealthCheckHandler;
import skycave.services.playerservice.healthcheck.HealthCheckHandler;

public class PlayerServiceWebHost {
    private final static Logger log = LoggerFactory.getLogger("PlayerServiceWebHost");

    public static void main(String[] args) throws Exception {
        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;
        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException ex) {
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            return;
        }

        // Help
        if(commandLine.hasOption('h')) {
            new HelpFormatter().printHelp("java -jar PlayerService.jar [OPTIONS]", options);
            return;
        }

        // Storage selection
        PlayerStorage storage = createStorage(commandLine);
        if (storage == null)
            return;

        log.info("Using SaboteurPlayerService with DelayService");
        DelayService delayService = new DefaultDelayService();
        PlayerService service = new SaboteurPlayerService(new DefaultPlayerService(storage), delayService);
        if (service == null)
            return;

        HealthCheckHandler healthCheckHandler = new DefaultHealthCheckHandler(delayService);
        new WebService(service, delayService, healthCheckHandler).start();
    }

    private static PlayerStorage createStorage(CommandLine commandLine) {
        if(!commandLine.hasOption('r')) {
            log.info("Using InMemory storage");
            return new FakePlayerStorage();
        }

        String value = commandLine.getOptionValue('r');
        String[] values = value.split(":");
        if (values.length != 2) {
            System.err.println("Invalid format for redis host: " + value);
            return null;
        }

        String host = values[0];
        int port;
        try {
            port = Integer.parseInt(values[1]);
        }
        catch(Exception ex) {
            System.err.println("Invalid format for redis port: " + value);
            return null;
        }

        log.info("Using Redis storage: {}:{}", host, port);
        return new RedisPlayerStorage(host, port);
    }

    private static Options createOptions() {
        Options options = new Options();
        options.addOption("h", "help", false, "Print this help text");

        Option storage = Option.builder("r")
                .longOpt("redis")
                .hasArg()
                .argName("host:port")
                .desc("Host for Redis storage. If not specified, InMemory-storage will be used.")
                .build();
        options.addOption(storage);

        return options;
    }
}
