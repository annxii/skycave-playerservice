package skycave.services.playerservice.main;

public class RequestError {
    private String errorMessage;

    public RequestError(String errorMessage, Object... args) {
        this(String.format(errorMessage, args));
    }

    public RequestError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() { return errorMessage; }
}
