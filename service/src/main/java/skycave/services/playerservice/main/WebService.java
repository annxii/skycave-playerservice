package skycave.services.playerservice.main;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skycave.services.playerservice.PlayerRecord;
import skycave.services.playerservice.PlayerService;
import skycave.services.playerservice.SavePlayerResponse;
import skycave.services.playerservice.ValidationException;
import skycave.services.playerservice.doubles.DelayService;
import skycave.services.playerservice.healthcheck.HealthCheckHandler;
import spark.Request;
import spark.Response;

import javax.servlet.http.HttpServletResponse;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;

import static spark.Spark.*;
import static spark.Spark.get;

public class WebService {

    private final static int port = 5000;
    private final static String MimeTypeJson = "application/json";
    private final static Logger log = LoggerFactory.getLogger("WebService");

    private final PlayerService playerService;
    private final DelayService delayService;
    private final HealthCheckHandler healthCheckHandler;

    public WebService(PlayerService playerService, DelayService delayService, HealthCheckHandler healthCheckHandler) {
        this.playerService = playerService;
        this.delayService = delayService;
        this.healthCheckHandler = healthCheckHandler;
    }

    public void start() {
        log.info("Starting PlayerService API at port {}", port);
        healthCheckHandler.start("PlayerService");
        port(port);

        before((req, res) -> {
            log.info(String.format("%s: %s\n%s", req.requestMethod(), req.uri(), req.body()));
        });

        path("/skycave/v1", () -> {
            get("/health", healthCheckHandler::getCurrentHealth, new JsonResponseTransformer());
            post("/delay/:ms", this::handleSetDelay, new JsonResponseTransformer());

            path("/players/:id", () -> {
                getJson(this::handleGetPlayer);
                postJson(this::handleUpdatePlayer);
            });
            path("/playersat/:position", () -> {
                getJson(this::handleGetPlayersAt);
            });
        });
    }

    private Object handleGetPlayer(Request request, Response response) throws Exception {
        String id = request.params(":id");
        PlayerRecord player = playerService.getPlayerByID(id);

        if (player == null) {
            response.status(HttpServletResponse.SC_NOT_FOUND);
            return createNotFound(response,"Player not found: %s", id);
        }

        return player;
    }

    private Object handleUpdatePlayer(Request request, Response response) throws Exception {
        // validate content type
        if (!MimeTypeJson.equals(request.contentType())) {
            return createBadRequest(response, "Unsupported content type: %s", request.contentType());
        }

        // deserialize json body
        PlayerRecord player;
        try {
            player = new Gson().fromJson(request.body(), PlayerRecord.class);
        }
        catch (JsonParseException ex) {
            return createBadRequest(response, ex.getMessage());
        }

        // perform upsert
        try {
            SavePlayerResponse result = playerService.savePlayer(player);
            response.status(result.getStatusCode());
            response.header("Location", request.uri());
            return result.getPlayer();
        }
        catch (ValidationException ex) {
            return createBadRequest(response, ex.getMessage());
        }
    }

    private Object handleGetPlayersAt(Request request, Response response) throws Exception {
        try {
            List<PlayerRecord> playersAt = playerService.getPlayersAtPosition(request.params(":position"));
            return playersAt;
        }
        catch (ValidationException ex) {
            return createBadRequest(response, ex.getMessage());
        }
    }

    private Object handleSetDelay(Request request, Response response) throws Exception {
        try {
            int ms = Integer.parseInt(request.params(":ms"));
            delayService.setDelay(ms);
            return null;
        }
        catch (NumberFormatException ex) {
            return createBadRequest(response, "Invalid amount of milliseconds specified: " + request.params(":ms"));
        }
    }

    private void postJson(spark.Route route) {
        post("", MimeTypeJson, (req, res) -> {
            res.type(MimeTypeJson);
            if (delayService.isDelayEnabled()) {
                res.header("X-Delay", delayService.getDelay() + "ms");
            }
            return route.handle(req, res);
        }, new JsonResponseTransformer());
    }

    private void getJson(spark.Route route) {
        get("", MimeTypeJson, (req, res) -> {
            res.type(MimeTypeJson);
            if (delayService.isDelayEnabled()) {
                res.header("X-Delay", delayService.getDelay() + "ms");
            }
            return route.handle(req, res);
        }, new JsonResponseTransformer());
    }

    private static RequestError createBadRequest(Response response, String message, Object... args) {
        return createRequestError(response, HttpServletResponse.SC_BAD_REQUEST, message, args);
    }

    private static RequestError createNotFound(Response response, String message, Object... args) {
        return createRequestError(response, HttpServletResponse.SC_NOT_FOUND, message, args);
    }

    private static RequestError createRequestError(Response response, int statusCode, String message, Object... args) {
        response.type(MimeTypeJson);
        response.status(statusCode);
        return new RequestError(message, args);
    }
}
