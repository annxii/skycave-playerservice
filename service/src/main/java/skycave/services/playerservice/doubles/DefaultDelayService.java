package skycave.services.playerservice.doubles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultDelayService implements DelayService {
    private final Logger log = LoggerFactory.getLogger("DelayService");
    private int milliseconds = 0;

    @Override
    public void delay() throws Exception {
        if (isDelayEnabled()) {
            log.info("Delaying response {}ms", milliseconds);
            Thread.sleep(milliseconds);
        }
    }

    @Override
    public void setDelay(int milliseconds) {
        this.milliseconds = milliseconds;
    }

    @Override
    public int getDelay() { return this.milliseconds; }

    @Override
    public boolean isDelayEnabled() {
        return milliseconds > 0;
    }
}
