package skycave.services.playerservice.doubles;

import skycave.services.playerservice.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FakePlayerStorage implements PlayerStorage {

    private final Map<String, PlayerRecord> playerMap = new HashMap<>();

    @Override
    public PlayerRecord getPlayerByID(String playerID) {
        return playerMap.get(playerID);
    }

    @Override
    public int updatePlayer(PlayerRecord record) {
        String id = record.getId();
        boolean exists = playerMap.containsKey(id);
        playerMap.put(id, record);

        if (exists) {
            return HttpServletResponse.SC_OK;
        }
        else {
            return HttpServletResponse.SC_CREATED;
        }
    }

    @Override
    public List<PlayerRecord> getPlayersAtPosition(String position) {
        return playerMap
                .values()
                .stream()
                .filter(x -> x.getPosition().equals(position))
                .collect(Collectors.toList());
    }
}
