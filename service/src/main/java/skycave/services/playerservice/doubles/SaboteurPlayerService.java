package skycave.services.playerservice.doubles;

import skycave.services.playerservice.PlayerRecord;
import skycave.services.playerservice.PlayerService;
import skycave.services.playerservice.SavePlayerResponse;

import java.util.List;

public class SaboteurPlayerService implements PlayerService {
    private final PlayerService innerService;
    private final DelayService delayService;

    public SaboteurPlayerService(PlayerService innerService, DelayService delayService) {
        this.innerService = innerService;
        this.delayService = delayService;
    }

    @Override
    public PlayerRecord getPlayerByID(String id) throws Exception {
        delayService.delay();
        return innerService.getPlayerByID(id);
    }

    @Override
    public List<PlayerRecord> getPlayersAtPosition(String position) throws Exception {
        delayService.delay();
        return innerService.getPlayersAtPosition(position);
    }

    @Override
    public SavePlayerResponse savePlayer(PlayerRecord player) throws Exception {
        delayService.delay();
        return innerService.savePlayer(player);
    }
}
