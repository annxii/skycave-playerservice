package skycave.services.playerservice.doubles;

public interface DelayService {
    void delay() throws Exception;
    void setDelay(int milliseconds);
    int getDelay();
    boolean isDelayEnabled();
}
