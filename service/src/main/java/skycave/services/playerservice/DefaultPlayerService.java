package skycave.services.playerservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultPlayerService implements PlayerService {
    private final Logger log = LoggerFactory.getLogger("PlayerService");
    private final PlayerStorage storage;

    public DefaultPlayerService(PlayerStorage storage) {
        this.storage = storage;
    }

    @Override
    public PlayerRecord getPlayerByID(String id) throws ValidationException {
        log.info("Requesting player: " + id);
        return storage.getPlayerByID(id);
    }

    @Override
    public List<PlayerRecord> getPlayersAtPosition(String position) throws ValidationException {
        log.info("Requesting players at: " + position);
        validatePosition(position);

        return storage.getPlayersAtPosition(position)
                .stream()
                .filter(x -> x.getAccessToken() != null)
                .collect(Collectors.toList());
    }

    @Override
    public SavePlayerResponse savePlayer(PlayerRecord player) throws ValidationException {
        log.info("Saving player: " + player.getId());

        validateRequiredValue("id", player.getId());
        validateRequiredValue("name", player.getName());
        validateRequiredValue("position", player.getPosition());

        validatePosition(player.getPosition());

        int statusCode = storage.updatePlayer(player);
        return new SavePlayerResponse(statusCode, player);
    }

    private  void validateRequiredValue(String property, String value) throws  ValidationException {
        if (value == null || value.equals("")) {
            throw  new ValidationException(String.format("%s not specified", property));
        }
    }

    private void validatePosition(String position) throws ValidationException {
        // invalid positions will fail parsing
        try {
            Point3.parseString(position);
        }
        catch (Exception ex) {
            throw new ValidationException("Invalid position: " + position);
        }
    }
}
