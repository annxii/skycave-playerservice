# SkyCave - From monolith to microservices

## 2021-10-30
- Getting an overview of the methods involved in PlayerService
  - This should be rather simple. Get, Get "list" and Update
  - Only data contract required should be PlayerRecord
- Separate public git repo
- Boilerplate project- and REST setup.
  - A bit hard to decide a structure....!
- Endpoint setup and working with JSON response

## 2021-10-31
- Wiring up Spark with application service (PlayerService)
  - Cannot decide how to handle errors...
  - Hard to get context into error codes when handling them using Spark's infrastructure
    - "notFound" and "internalServerError" seem too generic for my taste...
  - Throwing custom ValidationException from application and creating my own error-response.
    - works nicely!
- Adding simple "FakePlayerStorage" (copy/paste idea from monolith skycave fake storage
- First attempt at Docker
  - doing for a multi-step - makes so much more sense!
  - image worked at first try!!
- Storage should be configurable! Need command line inputs
  - Trying with an Apache command line package
  - This is great! Need to find me one of these for .NET for work!

## 2021-11-01
- API spec!
  - Pretty simple but should do...
- Starting CDT
  - Kinda hard to place correctly - Hidden away if inside monolith-project but build pipeline will get weird if it is contained in playerservice-project because the image will need to be build and pushed before CDT can take place!
- Need to restructure project ordering!
  - Gradle and java sucks!

## 2021-11-02
- Copy/pasting structure from monolith to fix project structure issues!
  - finally! it's working!
- Writing the actual CDT!
  - Pretty straight forward
  - TestContainers are awesome!

## 2021-11-03
- Starting to strangle SkyCave!
  - Starting with my own player service...
- Starting with a decorator and then replace calls as services become available
  - Hmm...decorator might not be a good idea - existing code has this weird initialization structure which does not work well with decorator...
  - Copy/paste all into strangler class instead
- Strangling storage! remove the required storage methods and see where everything breaks!

## 2021-11-04
- Refactoring based on feedback
- Feedback for other teams in strangler-group